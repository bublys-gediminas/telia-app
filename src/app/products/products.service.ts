import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {ProductCategoryModel} from './product-category.model';
import {Observable} from 'rxjs';
import {ProductModel} from './product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient: HttpClient) {}

  public getPhoneCategory(): Observable<ProductCategoryModel> {
    const endpoint = 'https://www.telia.se/.api/tse/dcnf-ipc-api-gateway/v1/phones';
    return this.httpClient.get<ProductCategoryModel>(endpoint)
      .pipe(
        map(c => new ProductCategoryModel(c))
      );
  }

  public isProductFavorite(product: ProductModel) {
    const favorites = sessionStorage.getItem('favorites');

    if (!favorites) {
      return false;
    }

    return favorites.includes(product.id);
  }

  public toggleFavorite(product: ProductModel) {
    if (!sessionStorage.getItem('favorites')) {
      sessionStorage.setItem('favorites', JSON.stringify([]));
    }

    let favorites = JSON.parse(sessionStorage.getItem('favorites')) as string[];

    if (favorites.includes(product.id)) {
      favorites = favorites.filter(id => id !== product.id);
    } else {
      favorites.push(product.id);
    }

    sessionStorage.setItem('favorites',  JSON.stringify(favorites));
  }
}
