export class ProductModel {

  id: string;
  displayName: string;
  displayImageUrl: string;

  constructor(props: any) {
    if (!props) { return; }

    Object.assign(this, props);
  }
}
