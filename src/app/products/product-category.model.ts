import {ProductModel} from './product.model';

export class ProductCategoryModel {
  public id: string;
  public headline: string;
  public options: ProductModel[];

  constructor(data: { id: string, headline: string, options: ProductModel[] }) {
    if (!data) { return; }

    Object.assign(this, data);
    this.options = this.options.map(p => new ProductModel(p));
  }
}
