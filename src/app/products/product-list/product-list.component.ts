import {Component, Input, OnInit} from '@angular/core';
import {ProductModel} from '../product.model';
import {ProductsService} from '../products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @Input() options: ProductModel[] = null;

  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {}

  public toggleFavorite(product: ProductModel) {
    this.productsService.toggleFavorite(product);
  }

  public isFavorite(product: ProductModel): boolean {
    return this.productsService.isProductFavorite(product);
  }

}
