import {Component, OnInit} from '@angular/core';
import {ProductsService} from './products/products.service';
import {ProductCategoryModel} from './products/product-category.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public productCategory: ProductCategoryModel = null;
  public hasError = false;

  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {
    this.productsService.getPhoneCategory()
      .subscribe(category => this.productCategory = category, () => {
        this.hasError = true;
      });
  }
}
